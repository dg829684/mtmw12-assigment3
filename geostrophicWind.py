# -*- coding: utf-8 -*-
"""
Student ID: dg829684

Assignment3_geostrophicWind
"""
import numpy as np
import matplotlib.pyplot as plt
from differentiate import*
from physProps import *

def geostrophicWind(N):
    """Calculate the geostrophic with analytically and numetrically and plot
    with resolution N"""
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    #The length of spacing
    dy = (ymax-ymin)/N
    #The spatial dimension ,y:
    y = np.linspace(ymin, ymax, N+1)
    #The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    #The pressure gradient and wind using two points difference
    dpdy = gradient_2point(p,dy)
    u_2point = geoWind(dpdy, physProps)
    error = u_2point - uExact
    return u_2point, uExact, error
    

    
#Graph to compare the numerical and analytic solutions
#Plot using large fonts
def windplotting(N):
    """Plotting the comparison between analytic and numerical solution and 
    their error"""
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    #The length of spacing
    dy = (ymax-ymin)/N
    #The spatial dimension ,y:
    y = np.linspace(ymin, ymax, N+1)
    uExact = geostrophicWind(N)[1]
    u_2point = geostrophicWind(N)[0]
    error = geostrophicWind(N)[2]
    
    font = {"size": 12}
    plt.rc('font', **font)
    #Plot the approximate and exact wind at y points
    fig = plt.figure()
    ax1 = fig.add_axes([0.05,0.1,0.7,0.8])
    ax2 = fig.add_axes([0.9,0.1,0.7,0.8])
    ax1.plot(y/1000, uExact, 'k-',label='Exact')
    ax1.plot(y/1000, u_2point, '*k--',label='Two-points differences',\
              ms=12, markerfacecolor='none')
    ax2.plot(y/1000,error,'k-')
    ax1.legend(loc='best')
    ax1.set_xlabel("y(km)")
    ax1.set_ylabel("u(m/s)")
    ax2.set_xlabel("y(km)")
    ax2.set_ylabel("u(m/s)")
    ax2.set_yticks(np.arange(-0.05,0.25,0.05))
    plt.show()
#Calculate the numerical, analytical solution and error when N=10
windplotting(N=10)

#test the order of accuracy at varying height location
def OrderofAccuracy(N):
    """Return of order of accuracy in the defined function wind() with two
    chosen number of segment N,2*N"""
    dy1 = (physProps["ymax"] - physProps["ymin"])/N
    dy2 = (physProps["ymax"] - physProps["ymin"])/(2*N)
    #define the array representing error for N1 and N2
    e1 = np.zeros(N+1)
    e2 = np.zeros_like(e1)
    #Calculate the error of wind speed at medium height
    Order = np.zeros_like(e1)
    #read the error at corresponding height from defined function wind()
    for i in range (0,N+1):
        e1[i] = geostrophicWind(N)[2][i]
        e2[i] = geostrophicWind(2*N)[2][2*i]
        #Calculate the order of accuracy
        if e1[i]>0 and e2[i]>0:
            Order[i]= (np.log(e1[i]) - np.log(e2[i]))/(np.log(dy1) - np.log(dy2))
        #if error is negtive, log(e)cannot be presented
        else:
            Order[i]= (np.log(-e1[i]) - np.log(-e2[i]))/(np.log(dy1) - np.log(dy2))
    return Order
#Test the order of accuracy when N1=10,N2=20
def OrderPlotting(N):
    """plotting the order of accuracy at varying height"""
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    y = np.linspace(ymin/1000, ymax/1000, N+1)
    Order = OrderofAccuracy(N)
    plt.plot(y,Order,'k*-',label='Order of accuracy')
    plt.xlabel('Height(km)')
    plt.ylabel('Order of Accuracy')
    plt.xticks(np.arange(0,1100,100))
    plt.show()
OrderPlotting(10)
        