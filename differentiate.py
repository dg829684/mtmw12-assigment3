# -*- coding: utf-8 -*-
"""
Student ID: dg829684

Assignment3_differentiate
"""
import numpy as np
#Functions for calculating gradients
def gradient_2point(f,dx):
    """The gradient of one dimensional array f assuming points are distance dx apart using
    2-point difference, returns an array the same size of f"""
    #Initialize the array for the gradient to be the same size of f
    dfdx = np.zeros_like(f)
    #Two points difference at the end point
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    #centred differences for the mid-points
    for i in range (1, len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx