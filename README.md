# MTMW12 Assigment3


The code for MTMW12 Assigment3

For question 1

Read the air properties and definication of some functions in file 'physProps.py' and 'differentiate.py'
Run the file 'geostrophicWind.py' to calculate and plot numerical and analytic wind and their error, also determine the order of accuracy at varying height

For question2

The difference method for calculating pressure gradient is updated in file'differentiate_modify.py
The updated wind and order of accuracy is calculated and plotted in file'geostrophicWind_modify.py